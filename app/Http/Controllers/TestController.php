<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Description of ClientController
 *
 * @author maxim
 */
class TestController {
    
    
    public function publish(Request $request, $topic)
    {
        //lets say it comes from DB or from config
        $subscribe_urls = [
            'http://localhost:8000/subscribe/'
        ];
        $result = [];
        foreach ($subscribe_urls as $url) {
            $result[$url] = $this->send($url.$topic, json_decode($request->getContent(), 1));
        }
        return $result;
    }
    
    public function subscribe(Request $request, $topic)
    {
        
        $forward_url = 'http://localhost:8000/event';
        
        return ['status' => $this->send($forward_url, [
            'topic' => $topic,
            'data' => json_decode($request->getContent(), 1)
        ])];
        
    }    
    
    private function send($url, $data) {
        $status = true;
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $output = curl_exec($ch);
            curl_close($ch);
        } catch (\Exception $e) {
            echo $e->getMessage();
            $status = false;
        }
        return $status;
    }
    
}

